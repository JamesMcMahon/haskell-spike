{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

import Data.ByteString (ByteString)
import Lib
import System.Directory
import System.IO
import Test.Hspec
import Text.RawString.QQ

main :: IO ()
main =
  hspec $ parallel $ do
    describe "doubleIt" $
      it "doubles the number passed in" $ do
        doubleIt 5 `shouldBe` 10
        doubleIt 6 `shouldBe` 12
    describe "hasBunny" $
      it "checks if string has bunny" $ do
        hasBunny "fluffy bunny" `shouldBe` True
        hasBunny "fluffy dog" `shouldBe` False
    describe "firstCaptureGroup" $
      it "returns the contents of the first capture group" $ do
        let regex = firstCaptureGroup ".*?\\((.*?)\\).+"
        regex "River City Ransom (U) [b3].nes" `shouldBe` "U"
        regex "Kokkun no Gourmet World (J) [T+Eng1.00_Hello].nes" `shouldBe` "J"
    describe "parseYaml" $ do
      it "returns expected data" $ do
        let (Just actual) =
              parseYaml
                [r|
                rattail: true
                stuff: bunch
                bucket:
                - apples
                - oranges
                |]
        rattail actual `shouldBe` True
        stuff actual `shouldBe` "bunch"
        bucket actual `shouldBe` ["apples", "oranges"]
      it "returns Nothing if data is wrong" $ do
        let actual =
              parseYaml
                [r|
                rattail: 7
                stuff: bunch
                bucket:
                - apples
                - oranges
                |]
        actual `shouldBe` Nothing
    describe "mvFile" $
      it "moves a file" $ do
        writeFile "test-file" "hasellhoff"
        mvFile "test-file" "moved-file"
        contents <- readFile "moved-file"
        removeFile "moved-file"
        contents `shouldBe` "hasellhoff"
