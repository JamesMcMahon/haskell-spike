{-# LANGUAGE OverloadedStrings #-}

module Lib where

import Data.ByteString (ByteString)
import qualified Data.Yaml as Y
import Data.Yaml (FromJSON(..), (.:))
import System.Directory
import Text.Regex.PCRE

someFunc :: IO ()
someFunc = putStrLn "someFunc"

doubleIt :: Integer -> Integer
doubleIt x = x * 2

hasBunny :: String -> Bool
hasBunny x = x =~ ("bunny" :: String)

firstCaptureGroup :: String -> String -> String
firstCaptureGroup regex str = last . head $ (str =~ regex :: [[String]])

data YamlTest = YamlTest
  { rattail :: Bool
  , stuff :: String
  , bucket :: [String]
  } deriving (Eq, Show)

instance FromJSON YamlTest where
  parseJSON (Y.Object v) =
    YamlTest <$> v .: "rattail" <*> v .: "stuff" <*> v .: "bucket"
  parseJSON _ = fail "Expected Object for YamlTest value"

parseYaml :: ByteString -> Maybe YamlTest
parseYaml a = Y.decode a :: Maybe YamlTest

-- Just an alias for a standard method
mvFile :: FilePath -> FilePath -> IO ()
mvFile = System.Directory.renameFile
